clc
clear
close all;
 
% Fill in vehicle parameters
vehicle(1).m          = 1670;       % kg
vehicle(1).Iz         = 2100;       % kg-m^2
vehicle(1).a          = 0.99;       % meters
vehicle(1).b          = 1.7;        % meters
vehicle(1).Caf        = -61600*2;   % N/rad;
vehicle(1).Car        = -52100*2;   % N/rad;

vehicle(2).m          = 650;        % kg
vehicle(2).Iz         = 850;        % kg-m^2
vehicle(2).a          = 1.63;       % meters
vehicle(2).b          = 1.18;       % meters
vehicle(2).Caf        = -140000;    % N/rad;
vehicle(2).Car        = -180000;    % N/rad;

vehicle(3).m          = 1640;       % kg
vehicle(3).Iz         = 3500;       % kg-m^2
vehicle(3).a          = 1.3;        % meters
vehicle(3).b          = 1.5;        % meters
vehicle(3).Caf        = -50000*2;   % N/rad;
vehicle(3).Car        = -80000*2;   % N/rad;


clear all_X all_Y all_r all_phi
for U = 30
  for i=1:3
   
    m = vehicle(i).m;
    Iz = vehicle(i).Iz;
    a = vehicle(i).a;
    b = vehicle(i).b;
    L = a+b;
    Caf = vehicle(i).Caf;
    Car = vehicle(i).Car;
    
    A11 = (Caf+Car) / (m*U);
    A12 = ((a*Caf - b*Car)/(m*U))-U;
    A21 = (a*Caf - b*Car) / (Iz*U);
    A22 = (((a^2)*Caf) + ((b^2)*Car)) / (Iz*U);

    A   = [A11 A12; A21 A22];
    
    B11 = -Caf/m;
    B12 = -Car/m;
    B21 = -((a*Caf)/Iz);
    B22 = ((b*Car)/Iz);
    B   = [B11 B12; B21 B22];
    
    C = eye(2);
    D = zeros(2);
    
    
   
%     sim('ME_452_HW10_2.mdl',20);
%     
%     h1 = figure(77);
%     set(h1,'Name','XY Position')
%     plot(X_int,Y_int,'color',[rand rand rand]);
%     title('Daniel Considine');
%     xlabel('X Position (m)');
%     ylabel('Y Position (m)');
%     legend('1','2','3','Location','SouthEast')
%     hold on;
    
    sim('ME_452_HW10_1.mdl',75);
  
    
    h2 = figure(88);
    set(h2,'Name','XY Position')
    plot(X_int,Y_int,'color',[rand rand rand]);
    title('Daniel Considine');
    xlabel('X Position (m)');
    ylabel('Y Position (m)');
    legend('1','2','3','Location','SouthEast')
    hold on;
    
  end
end