close all;
clear all;

U = 20;
vehicle(1).m          = 1031; % kg
vehicle(1).Iz         = 1886; % kg-m^2
vehicle(1).a          = 0.927; % meters
vehicle(1).b          = 1.562;  % meters
vehicle(1).Caf        = -2*53663; % N/rad;
vehicle(1).Car        = -2*45231; % N/rad;
 

figure(1)
Tfinal = 20;
clear all_X all_Y all_r all_phi
vehicle_num = 1;

% Initialize arrays
velocities = [8:1:60];
real_values1 = 0*velocities;
real_values2 = 0*velocities;

i = 1;

for U=velocities
    m = vehicle(vehicle_num).m;
    Iz = vehicle(vehicle_num).Iz;
    a = vehicle(vehicle_num).a;
    b = vehicle(vehicle_num).b;
    L = a+b;
    Caf = vehicle(vehicle_num).Caf;
    Car = vehicle(vehicle_num).Car;
    K = -0.01;
    g = 9.81;
    
    Car =-1* m*g*a/((m*g*b/(-1*Caf))-K*L)
    
    % Calculate the understeer gradient to check work above
    K = -1*(m*g*b/(Caf*L)-m*g*a/(Car*L))
    
    % Calculate the critical velocity
    U_crit = sqrt( -Caf*Car*(a+b)^2/(m*(a*Caf-b*Car)))
    disp('paused');
    %pause;
 
    A = [(Caf+Car)/(m*U) (a*Caf-b*Car)/(m*U)-U;
        (a*Caf-b*Car)/(Iz*U) (a^2*Caf+b^2*Car)/(Iz*U)];
    B = [-Caf/m -Car/m; -a*Caf/Iz b*Car/Iz];
    
    E = eig(A);
    plot(real(E),imag(E),'bx'); hold on;  
    xlabel('real portion (rad/sec)');
    ylabel('imag portion (rad/sec)');
    
    real_portions = real(E);
    real_values1(i) = real_portions(1);
    real_values2(i) = real_portions(2);
    
    pause(0.1);
    i = i+1;
end

% Make the plot of velocity versus real portion
figure(2);
plot(velocities,real_values1,'b-',velocities,real_values2,'r-'); 
grid on;
xlabel('velocity (m/s)');
ylabel('real portion (rad/sec)');
