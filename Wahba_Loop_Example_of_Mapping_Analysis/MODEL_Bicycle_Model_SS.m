clear all
close all
clc

g = 9.81;
 
m_taurus = 1670;
Iz_taurus = 2100;
a_taurus = 0.99;
b_taurus = 1.7;
Caf_taurus = -61600*2;
Car_taurus = -52100*2;

a = a_taurus;
b = b_taurus;
L = a+b;
m = m_taurus;
Caf = Caf_taurus;
Car = Car_taurus;

Ff = m*g * b/L;
Fr = m*g * a/L;

C_alphaf = Caf/Ff
C_alphar = Caf/Fr


m_indy = 650;
Iz_indy = 850;
a_indy = 1.63;
b_indy = 1.18;
Caf_indy = -140000;
Car_indy = -180000;

a = a_indy;
b = b_indy;
L = a+b;
m = m_indy;
Caf = Caf_indy;
Car = Car_indy;

Ff = m*g * b/L;
Fr = m*g * a/L;

C_alphaf = Caf/Ff
C_alphar = Caf/Fr


m_benz = 1640;
Iz_benz = 3500;
a_benz = 1.3;
b_benz = 1.5;
Caf_benz = -50000*2;
Car_benz = -80000*2;

a = a_benz;
b = b_benz;
L = a+b;
m = m_benz;
Caf = Caf_benz;
Car = Car_benz;

Ff = m*g * b/L;
Fr = m*g * a/L;

C_alphaf = Caf/Ff
C_alphar = Caf/Fr


 
U = 20;
 
A11 = (Caf_taurus+Car_taurus)/(m_taurus*U);
A12 = ((a_taurus*Caf_taurus-b_taurus*Car_taurus)/(m_taurus*U))-U;
A21 = (a_taurus*Caf_taurus-b_taurus*Car_taurus)/(Iz_taurus*U);
A22 = (a_taurus^2*Caf_taurus+b_taurus^2*Car_taurus)/(Iz_taurus*U);
 
B11 = -Caf_taurus/m_taurus;
B12 = -Car_taurus/m_taurus;
B21 = -a_taurus*Caf_taurus/Iz_taurus;
B22 = b_taurus*Car_taurus/Iz_taurus;
 
A = [A11 A12; A21 A22];
B = [B11; B21];
C = eye(2);
D = zeros(2,1);
 
sim('MODEL_Bicycle_Model_SS_Turn')
 
figure(1)
plot(X,Y)
hold on
grid on
 
A11 = (Caf_indy+Car_indy)/(m_indy*U);
A12 = ((a_indy*Caf_indy-b_indy*Car_indy)/(m_indy*U))-U;
A21 = (a_indy*Caf_indy-b_indy*Car_indy)/(Iz_indy*U);
A22 = (a_indy^2*Caf_indy+b_indy^2*Car_indy)/(Iz_indy*U);
 
B11 = -Caf_indy/m_indy;
B12 = -Car_indy/m_indy;
B21 = -a_indy*Caf_indy/Iz_indy;
B22 = b_indy*Car_indy/Iz_indy;
 
A = [A11 A12; A21 A22];
B = [B11; B21];
C = eye(2);
D = zeros(2,1);
 
sim('MODEL_Bicycle_Model_SS_Turn')
figure(1)
plot(X,Y, 'r')
hold on
grid on
 
A11 = (Caf_benz+Car_benz)/(m_benz*U);
A12 = ((a_benz*Caf_benz-b_benz*Car_benz)/(m_benz*U))-U;
A21 = (a_benz*Caf_benz-b_benz*Car_benz)/(Iz_benz*U);
A22 = (a_benz^2*Caf_benz+b_benz^2*Car_benz)/(Iz_benz*U);
 
B11 = -Caf_benz/m_benz;
B12 = -Car_benz/m_benz;
B21 = -a_benz*Caf_benz/Iz_benz;
B22 = b_benz*Car_benz/Iz_benz;
 
A = [A11 A12; A21 A22];
B = [B11; B21];
C = eye(2);
D = zeros(2,1);
 
sim('MODEL_Bicycle_Model_SS_Turn')
figure(1)
plot(X,Y, 'g')
hold on
grid on
xlabel('Global X (m)')
ylabel('Global Y (m)')
text(205,155,'Indy Car')
text(255,85,'Ford Taurus')
text(255,70,'Benz E-Class')
