function rawData = fcn_loadRawData(filename,variable_names)

% This function is used to load the mapping van DGPS data collected for the
% Wahba route on 2019_09_17 with the Penn State Mapping Van.
%
% Author: Sean Brennan
% Date: 2019_10_03
% modify Date: 2019_10_20
% 
% Updates:
%  2019_10_03 - Dr. Brennan revised from Liming Gao's prior version to
%  comparmentalize this data loading to a structure
%  2019_10_17 - removed bad indices from navMode. This was causing problems
%  in later automated processing, in the time alignment, as it refers to
%  indices that are changing during the time alignment step.
%  2019_10_20 - added debug mode to make the function more verbose.

flag_do_debug = 1;

if flag_do_debug
    % Grab function name
    st = dbstack;
    namestr = st.name;

    % Show what we are doing
    fprintf(1,'\nWithin function: %s\n',namestr);
    fprintf(1,'Starting load of rawData structure from source files.\n');    
end

%% Load the data
data{1}.filename = filename;% 'Route_Wahba.mat';
data{1}.variable_names ={variable_names}; % {'Route_WahbaLoop'};

for i_data = 1:length(data)
    ith_filename = data{i_data}.filename;
    ith_variable_name = data{i_data}.variable_names{1}; % Need to do this as a loop if more than one variable
    if flag_do_debug
        % Show what we are doing
        fprintf(1,'Source file: %s is being used to load variable %s\n',ith_filename,ith_variable_name);
    end
    data_name = load(ith_filename,ith_variable_name);
end
    data_struct = data_name.( ith_variable_name); %Accessing Data Using Dynamic Field Names

    %% Process data from the GPS_2019 mat file - This is the Hemisphere
d = data_struct.Hemisphere_DGPS; % Create a temporary data structure

Hemisphere.ROS_Time         = d.Time';
Hemisphere.Npoints          = length(Hemisphere.ROS_Time(:,1));
Hemisphere.EmptyVector      = fcn_fillEmptyStructureVector(Hemisphere); % Fill in empty vector (this is useful later)

Hemisphere.GPS_Time         = d.GPSTimeOfWeek';
Hemisphere.Latitude         = d.Latitude';
Hemisphere.Longitude        = d.Longitude';
Hemisphere.Altitude         = d.Height';
Hemisphere.xEast            = d.xEast';
Hemisphere.yNorth           = d.yNorth';
Hemisphere.zUp              = d.zUp';
Hemisphere.velNorth         = d.VNorth';
Hemisphere.velEast          = d.VEast';
Hemisphere.velUp            = d.VUp';
Hemisphere.velMagnitude     = sqrt(Hemisphere.velNorth.^2 + Hemisphere.velEast.^2 + Hemisphere.velUp.^2); 
Hemisphere.numSatellites    = Hemisphere.EmptyVector;
Hemisphere.DGPS_is_active   = (d.NavMode==6)';
Hemisphere.Roll_deg         = Hemisphere.EmptyVector;
Hemisphere.Pitch_deg        = Hemisphere.EmptyVector;
Hemisphere.Yaw_deg          = Hemisphere.EmptyVector;
Hemisphere.Yaw_deg_Sigma    = Hemisphere.EmptyVector;
Hemisphere.OneSigmaPos      = d.StdDevResid'; 

% Calculate the apparent yaw angle from ENU positions
Hemisphere = fcn_fillRawYawEstimatesFromGPSPosition(Hemisphere);

% Estimate the variance associated with the estimated yaw
Hemisphere.Yaw_deg_from_position_Sigma = fcn_predictYawSigmaFromPosition(Hemisphere.xy_increments);

% Estimate the variance associated with the estimated yaw based on velocity
Hemisphere.Yaw_deg_from_position_Sigma = 0*Hemisphere.xy_increments;  % Initialize array to zero
good_indices = find(Hemisphere.DGPS_is_active==1);
Hemisphere.Yaw_deg_from_position_Sigma(good_indices) = ...
    fcn_predictYawSigmaFromPosition(Hemisphere.xy_increments(good_indices))/5;
bad_indices = find(Hemisphere.DGPS_is_active==0);
Hemisphere.Yaw_deg_from_position_Sigma(bad_indices)...
    = fcn_predictYawSigmaFromPosition(Hemisphere.xy_increments(bad_indices));

% Calculate the apparent yaw angle from ENU velocities
Hemisphere = fcn_fillRawYawEstimatesFromGPSVelocity(Hemisphere);


% Estimate the variance associated with the estimated yaw based on velocity
speeds = (Hemisphere.velNorth.^2+Hemisphere.velEast.^2).^0.5;
filt_speeds = medfilt1(speeds,20,'truncate');
Hemisphere.Yaw_deg_from_velocity_Sigma = 0*filt_speeds;  % Initialize array to zero
good_indices = find(Hemisphere.DGPS_is_active==1);
Hemisphere.Yaw_deg_from_velocity_Sigma(good_indices) = ...
    fcn_predictYawSigmaFromVelocity(filt_speeds(good_indices),0.005);
bad_indices = find(Hemisphere.DGPS_is_active==0);
Hemisphere.Yaw_deg_from_velocity_Sigma(bad_indices)...
    = fcn_predictYawSigmaFromVelocity(filt_speeds(bad_indices),0.5);

if 1==0
    figure(48448);
    h_1 = subplot(2,1,1);
    plot(Hemisphere.Yaw_deg_from_velocity);
    h_2 = subplot(2,1,2);
    plot(Hemisphere.DGPS_is_active);
    linkaxes([h_1 h_2],'x');
end



rawData.GPS_Hemisphere = Hemisphere;

%% Process data from Novatel
d = data_struct.GPS_Novatel; % Create a temporary data structure

GPS_Novatel.ROS_Time       = d.Time';
GPS_Novatel.Npoints        = length(GPS_Novatel.ROS_Time(:,1));
GPS_Novatel.EmptyVector    = fcn_fillEmptyStructureVector(GPS_Novatel); % Fill in empty vector (this is useful later)
GPS_Novatel.GPS_Time       = d.Seconds';
GPS_Novatel.Latitude       = d.Latitude';
GPS_Novatel.Longitude      = d.Longitude';
GPS_Novatel.Altitude       = d.Height';
GPS_Novatel.xEast          = d.xEast';
GPS_Novatel.yNorth         = d.yNorth';
GPS_Novatel.zUp            = d.zUp';
GPS_Novatel.velNorth       = d.NorthVelocity';
GPS_Novatel.velEast        = d.EastVelocity';
GPS_Novatel.velUp          = d.UpVelocity';
GPS_Novatel.velMagnitude   = sqrt(d.NorthVelocity'.^2+d.EastVelocity'.^2);

GPS_Novatel.numSatellites  = GPS_Novatel.EmptyVector;
GPS_Novatel.navMode        = GPS_Novatel.EmptyVector;
GPS_Novatel.Roll_deg       = d.Roll';
GPS_Novatel.Pitch_deg      = d.Pitch';
GPS_Novatel.Yaw_deg        = -d.Azimuth'+360+90; % Notice sign flip and phase shift due to coord convention and mounting
GPS_Novatel.Yaw_deg_Sigma  = 0.1 * ones(GPS_Novatel.Npoints,1); % Units are deg. Constant due to IMU on Novatel
GPS_Novatel.OneSigmaPos    = 0.10; % Typical 1-sigma in position, in meters



% Calculate the apparent yaw angle from ENU positions
GPS_Novatel = fcn_fillRawYawEstimatesFromGPSPosition(GPS_Novatel);

% Estimate the variance associated with the estimated yaw
GPS_Novatel.Yaw_deg_from_position_Sigma = 0.5*ones(GPS_Novatel.Npoints,1);


% Calculate the apparent yaw angle from ENU velocities
GPS_Novatel = fcn_fillRawYawEstimatesFromGPSVelocity(GPS_Novatel);

% Estimate the variance associated with the estimated yaw
GPS_Novatel.Yaw_deg_from_velocity_Sigma = 0.1*ones(GPS_Novatel.Npoints,1);

rawData.GPS_Novatel = GPS_Novatel;

%% Process data from the Garmin
d = data_struct.Garmin_GPS; % Create a temporary data structure

GPS_Garmin.ROS_Time       = d.Time';
GPS_Garmin.Npoints        = length(GPS_Garmin.ROS_Time(:,1));
deltaTSample              = mean(diff(GPS_Garmin.ROS_Time));
GPS_Garmin.EmptyVector    = fcn_fillEmptyStructureVector(GPS_Garmin); % Fill in empty vector (this is useful later)

GPS_Garmin.GPS_Time       = d.Time'*NaN;
GPS_Garmin.Latitude       = d.Latitude';
GPS_Garmin.Longitude      = d.Longitude';
GPS_Garmin.Altitude       = d.Height';
GPS_Garmin.xEast          = d.xEast';
GPS_Garmin.yNorth         = d.yNorth';
GPS_Garmin.zUp            = d.zUp';
GPS_Garmin.velNorth       = [0; diff(GPS_Garmin.yNorth)]/deltaTSample;
GPS_Garmin.velEast        = [0; diff(GPS_Garmin.xEast)]/deltaTSample;
GPS_Garmin.velUp          = [0; diff(GPS_Garmin.zUp)]/deltaTSample;
GPS_Garmin.velMagnitude   = sqrt(GPS_Garmin.velNorth.^2+GPS_Garmin.velEast.^2);
GPS_Garmin.numSatellites  = GPS_Garmin.EmptyVector;
GPS_Garmin.navMode        = GPS_Garmin.EmptyVector;
GPS_Garmin.Roll_deg       = GPS_Garmin.EmptyVector;
GPS_Garmin.Pitch_deg      = GPS_Garmin.EmptyVector;
GPS_Garmin.Yaw_deg        = GPS_Garmin.EmptyVector;
GPS_Garmin.Yaw_deg_Sigma  = GPS_Garmin.EmptyVector;
GPS_Garmin.OneSigmaPos    = 3; % Typical 1-sigma in position, in meters
GPS_Garmin.EmptyVector    = fcn_fillEmptyStructureVector(GPS_Garmin); % Fill in empty vector (this is useful later)

% Calculate the apparent yaw angle from ENU positions
GPS_Garmin = fcn_fillRawYawEstimatesFromGPSPosition(GPS_Garmin);

% Estimate the variance associated with the estimated yaw (Note: the Garmin
% is about 300 times less accurate than the Hemisphere, hence the 300
% factor.
GPS_Garmin.Yaw_deg_from_position_Sigma = fcn_predictYawSigmaFromPosition(GPS_Garmin.xy_increments/300);

% Calculate the apparent yaw angle from ENU velocities
GPS_Garmin = fcn_fillRawYawEstimatesFromGPSVelocity(GPS_Garmin);

% Estimate the variance associated with the estimated yaw
GPS_Garmin.Yaw_deg_from_velocity_Sigma = 100*ones(GPS_Garmin.Npoints,1);

rawData.GPS_Garmin = GPS_Garmin;

%% Process data from the Novatel IMU
d = data_struct.Novatel_IMU; % Create a temporary data structure
IMU_Novatel.ROS_Time      = d.Time';
IMU_Novatel.Npoints       = length(IMU_Novatel.ROS_Time(:,1));
IMU_Novatel.EmptyVector   = fcn_fillEmptyStructureVector(IMU_Novatel); % Fill in empty vector (this is useful later)
IMU_Novatel.GPS_Time      = d.Seconds';
IMU_Novatel.deltaT_ROS    = mean(diff(IMU_Novatel.ROS_Time));
IMU_Novatel.deltaT_GPS    = mean(diff(IMU_Novatel.GPS_Time));
IMU_Novatel.IMUStatus     = d.IMUStatus';
IMU_Novatel.XAccel        = d.XAccel';
IMU_Novatel.YAccel        = d.YAccel';
IMU_Novatel.ZAccel        = d.ZAccel';
IMU_Novatel.XGyro         = d.XGyro';
IMU_Novatel.YGyro         = d.YGyro';
IMU_Novatel.ZGyro         = d.ZGyro';

rawData.IMU_Novatel = IMU_Novatel;

%% Process data from the ADIS IMU
d = data_struct.adis_IMU_data; % Create a temporary data structure
IMU_ADIS.ROS_Time      = d.Time';
IMU_ADIS.Npoints       = length(IMU_ADIS.ROS_Time(:,1));
IMU_ADIS.EmptyVector   = fcn_fillEmptyStructureVector(IMU_ADIS); % Fill in empty vector (this is useful later)
IMU_ADIS.GPS_Time      = IMU_ADIS.EmptyVector;
IMU_ADIS.deltaT_ROS    = mean(diff(IMU_ADIS.ROS_Time));
IMU_ADIS.deltaT_GPS    = mean(diff(IMU_ADIS.GPS_Time));
IMU_ADIS.IMUStatus     = IMU_ADIS.EmptyVector;
IMU_ADIS.XAccel        = -d.LinearAccelerationY';
IMU_ADIS.YAccel        = d.LinearAccelerationX';
IMU_ADIS.ZAccel        = d.LinearAccelerationZ';
IMU_ADIS.XGyro         = d.AngularVelocityY';  % note - these seem to be swapped!?! (if enter them swapped, they seem to agree)
IMU_ADIS.YGyro         = d.AngularVelocityX';
IMU_ADIS.ZGyro         = d.AngularVelocityZ';

rawData.IMU_ADIS = IMU_ADIS;

%% Process data from the steering sensor - the sensor stinks, so we won't use it
d = data_struct.Steering_angle; % Create a temporary data structure

Input_Steering.ROS_Time        = d.Time';
Input_Steering.Npoints         = length(Input_Steering.ROS_Time(:,1));
Input_Steering.EmptyVector     = fcn_fillEmptyStructureVector(Input_Steering); % Fill in empty vector (this is useful later)
Input_Steering.GPS_Time        = Input_Steering.EmptyVector;
Input_Steering.deltaT_ROS      = mean(diff(Input_Steering.ROS_Time));
Input_Steering.deltaT_GPS      = mean(diff(Input_Steering.GPS_Time));
Input_Steering.LeftAngle       = -1*d.LeftAngle;
Input_Steering.RightAngle      = 1*d.RightAngle;
Input_Steering.Angle           = d.Angle;
Input_Steering.LeftCountsFilt  = d.LeftCountsFiltered;
Input_Steering.RightCountsFilt = d.RightCountsFiltered;

if 1==0  % For debugging - to compare steering input to yaw rate (should be linear)
    figure(34547);
    fcn_plotAllYawRateSources(rawData,34547,'All yaw rate sources')
    p1 = gca;
    
    t = Input_Steering.ROS_Time;
    figure(757557);
    clf;
    % plot(...
    %     t,Input_Steering.RightAngle,'r',...
    %     t,Input_Steering.LeftAngle,'b',...
    %     t,Input_Steering.Angle,'g');
    % plot(...
    %     t,Input_Steering.RightCountsFilt,'r',...
    %     t,Input_Steering.LeftCountsFilt,'b');
    plot(...
        t,-Input_Steering.RightCountsFilt+Input_Steering.LeftCountsFilt,'b');
    
    p2 = gca;
    linkaxes([p1,p2],'x')
end
rawData.Input_Steering = Input_Steering;

%% Process data from the wheel encoders
% Note: left encoder looks disconnected, and counts on both are not working

d = data_struct.Raw_encoder; % Create a temporary data structure

Encoder_RearWheels.ROS_Time             = d.Time';
Encoder_RearWheels.Npoints              = length(Encoder_RearWheels.ROS_Time(:,1));
Encoder_RearWheels.EmptyVector          = fcn_fillEmptyStructureVector(Encoder_RearWheels); % Fill in empty vector (this is useful later)
Encoder_RearWheels.GPS_Time             = Encoder_RearWheels.EmptyVector;
Encoder_RearWheels.deltaT_ROS           = mean(diff(Encoder_RearWheels.ROS_Time));
Encoder_RearWheels.deltaT_GPS           = mean(diff(Encoder_RearWheels.GPS_Time));
Encoder_RearWheels.CountsL              =  d.CountsL';
Encoder_RearWheels.CountsR              = d.CountsR';
Encoder_RearWheels.AngularVelocityL     = d.AngularVelocityL';
Encoder_RearWheels.AngularVelocityR     = d.AngularVelocityR';
Encoder_RearWheels.DeltaCountsL         = d.DeltaCountsL';
Encoder_RearWheels.DeltaCountsR         = d.DeltaCountsR';
%Encoder_RearWheels.DeltaCountsR        = [0; diff(Encoder_RearWheels.CountsR)];

% Calculate the wheel radius, on average
t = rawData.GPS_Novatel.ROS_Time;
V = rawData.GPS_Novatel.velMagnitude;
t_enc = Encoder_RearWheels.ROS_Time;  % encoder time
w = abs(Encoder_RearWheels.AngularVelocityR);
V_enc = interp1(t,V,t_enc,'nearest','extrap'); % velocity in encoder time
Encoder_RearWheels.RadiusAveR_in_meters = w'*w\(w'*V_enc);  
Encoder_RearWheels.VelocityR            = Encoder_RearWheels.RadiusAveR_in_meters*abs(Encoder_RearWheels.AngularVelocityR);

% Calculate the standard deviation in velocity prediction 
error = Encoder_RearWheels.VelocityR - V_enc;
% For debugging
% figure; hist(error,10000);
Encoder_RearWheels.VelocityR_Sigma      = std(error);
Encoder_RearWheels.velMagnitude         = Encoder_RearWheels.VelocityR;  
Encoder_RearWheels.velMagnitude_Sigma   = Encoder_RearWheels.VelocityR_Sigma;  


%%%% The following was to check to see if tire compressibility was a
%%%% factor...
% % Calculate the wheel radius, with compressibility of tire, k
% % Model is: vel = (r_wheel + v*yawrate*k)*w
% t_yawrate = rawData.IMU_Novatel.ROS_Time;
% yawrate = rawData.IMU_Novatel.ZGyro;
% yawrate_enc = interp1(t_yawrate,yawrate,t_enc,'nearest','extrap');
% w_new = [w V_enc.*yawrate_enc];
% solution = w_new'*w_new\(w_new'*V_enc);
% radius = solution(1);
% k = solution(2);
% 
% Encoder_RearWheels.VelocityR_with_compression = (radius + k*V_enc.*yawrate_enc).*abs(Encoder_RearWheels.AngularVelocityR);
% error2 = Encoder_RearWheels.VelocityR_with_compression - V_enc;
% std(error2)
% % For debugging
% figure(4); hist(error2,10000);


if 1==0  % For debugging - to compare steering input to yaw rate (should be linear)
    figure(46464);
    hold on;
    plot(t_enc,Encoder_RearWheels.VelocityR,'r');
    plot(t,V,'k');

    %%% The following plots were used to show that the counts are not right
    %     figure(34547);
    %
    %     subplot(2,1,1);
    %     plot(t,V,'k');
    %     p1 = gca;
    %
    %     subplot(2,1,2);
    %     %     plot(...
    %     %         t,Encoder_RearWheels.AngularVelocityR,'r',...
    %     %         t,Encoder_RearWheels.AngularVelocityL,'b');
    %     plot(...
    %         t2,w,'r');
    %     %     plot(...
    %     %         t,Encoder_RearWheels.DeltaCountsR,'r');
    %     p2 = gca;
    %     linkaxes([p1,p2],'x')
end

rawData.Encoder_RearWheels = Encoder_RearWheels;


% %% Process data from the Route_Wahba.mat file
% steeringAngleTime = ith_variable_name.Steering_angle.Time - ...
%     ith_variable_name.Steering_angle.Time(1);
% steeringAngleLeft_in_deg = ith_variable_name.Steering_angle.LeftAngle*180/pi;
% steeringAngleRight_in_deg = ith_variable_name.Steering_angle.RightAngle*180/pi;
% steeringAngle_in_deg = ith_variable_name.Steering_angle.Angle*180/pi;
% 
% % Plot results?Rou
% h_fig = figure(16262);
% set(h_fig,'Name','Raw_yaw_angle_in_deg');
% p1 = subplot(2,1,1);
% plot(steeringAngleTime,...
%     steeringAngleLeft_in_deg,'b'); hold on;
% p2 = subplot(2,1,2);
% plot(rawTime,...
%     [0; diff(yaw_angles_in_deg_from_velocity)],'k'); hold on;
% 
% linkaxes([p1,p2],'x')

%% Close out the loading process
if flag_do_debug
    % Show what we are doing
    fprintf(1,'\nFinished processing function: %s\n',namestr);
end

end

%%
function EmptyVector = fcn_fillEmptyStructureVector(structure)
EmptyVector = NaN*structure.ROS_Time;
end

%%
function structure = fcn_fillRawYawEstimatesFromGPSPosition(structure)

% Find the position increments first

    structure.xEast_increments = [0; diff(structure.xEast)];
    structure.yNorth_increments = [0; diff(structure.yNorth)];

structure.xy_increments = ...
    (structure.xEast_increments.^2+structure.yNorth_increments.^2).^0.5;  %distance in meters

structure.Yaw_deg_from_position = ...
    atan2d(structure.yNorth_increments, structure.xEast_increments );

% Fix yaw angles to be positive numbers (0 to 360);
neg_yaw_indices = find(structure.Yaw_deg_from_position<0);
structure.Yaw_deg_from_position(neg_yaw_indices) = ...
    structure.Yaw_deg_from_position(neg_yaw_indices)+360;
end

%%
function structure = fcn_fillRawYawEstimatesFromGPSVelocity(structure)

structure.Yaw_deg_from_velocity = ...
    atan2d(structure.velNorth,structure.velEast);

% Fix yaw angles to be positive numbers (0 to 360);
neg_yaw_indices = find(structure.Yaw_deg_from_velocity<0);
structure.Yaw_deg_from_velocity(neg_yaw_indices) = ...
    structure.Yaw_deg_from_velocity(neg_yaw_indices)+360;


end

