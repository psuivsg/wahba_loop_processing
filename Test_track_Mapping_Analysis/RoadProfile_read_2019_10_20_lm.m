% This program is used to plot the mapping va data collected for the
% test track  on 2019_10_18 with the Penn State Mapping Van.
%
% Author: Liming Gao
% Create Date: 2019_10_20
% raw data  offset only 

%% Clear the workspace
%clear all
%close all


%% define route name 
    route_name = 1; % 1 means 'test_track';  2 means wahba_loop; 

%% Define what is plotted
plottingFlags.flag_plot_Garmin = 0;

plottingFlags.fields_to_plot = [...    
    {'All_AllSensors_xEast'},...
    {'xEast_increments'},...
    {'All_AllSensors_xEast_increments'},...
    {'All_AllSensors_DGPS_is_active'},...
    ]; 

% THE TEMPLATE FOR ALL PLOTTING
% plottingFlags.fields_to_plot = [...    
%     {'Yaw_deg'},...                 % Yaw variables
%     {'Yaw_deg_from_position'},... 
%     {'Yaw_deg_from_velocity'},... 
%     {'All_SingleSensor_Yaw_deg'},... 
%     {'All_AllSensors_Yaw_deg'},...
%     {'ZGyro'},...                   % Yawrate (ZGyro) variables
%     {'All_AllSensors_ZGyro'},... 
%     {'velMagnitude'},...            % velMagnitude variables
%     {'All_AllSensors_velMagnitude'},...        
%     {'XAccel'},...                  % XAccel variables
%     {'All_AllSensors_XAccel'},...
%     {'xEast_increments'},...        % Position increment variables
%     {'All_AllSensors_xEast_increments'},...
%     {'yNorth_increments'},...
%     {'All_AllSensors_yNorth_increments'},...
%     {'XYplot'},...                  % XY plots
%     {'All_AllSensors_XYplot'},...
%     {'xEast'},...                   % xEast and yNorth plots
%     {'All_AllSensors_xEast'},...
%     {'yNorth'},...
%     {'All_AllSensors_yNorth'},...
%     {'DGPS_is_active'},...
%     {'All_AllSensors_DGPS_is_active'},...
%     {'velNorth'},...                % Remaining are not yet plotted - just kept here for now as  placeholders
%     {'velEast'},...
%     {'velUp'},...
%     {'Roll_deg'},...
%     {'Pitch_deg'},...  
%     {'xy_increments'}... % Confirmed
%     {'YAccel'},...
%     {'ZAccel'},...
%     {'XGyro'},...
%     {'YGyro'},...
%     ];

%% Define zoom points for plotting
plottingFlags.XYZoomPoint = [-4426.14413504648 -4215.78947791467 1601.69022519862 1709.39208889317];
% plottingFlags.TimeZoomPoint = [297.977909295872          418.685505549775];
% plottingFlags.TimeZoomPoint = [1434.33632953011          1441.17612419014];
plottingFlags.TimeZoomPoint = [1360   1450];
%% Load the raw data
% This data will have outliers, be unevenly sampled, have multiple and
% inconsistent measurements of the same variable. In other words, it is the
% raw data.
try
    temp = rawdata.tripTime(1);
catch
    if route_name== 1  % 1 means 'test_track';  2 means wahba_loop; 
        filename  = 'Route_test_track_10182019.mat';
        variable_names = 'Route_test_track';
    elseif route_name== 2
        filename  = 'Route_Wahba.mat';
        variable_names = 'Route_WahbaLoop';
    end
    rawData = fcn_loadRawData(filename,variable_names);
end
fcn_plotStructureData(rawData,plottingFlags);


%% data clean and merge 



%% start point of s-coordinate 
     %plot and define the start point by observing 
     start_index= 2400;
     %[start_longitude,start_latitude, start_xEast,start_yNorth] = fcn_plotraw_lla(rawData.GPS_Hemisphere, 1254, 'lla_raw_data', start_index);
     %fcn_googleEarth('test_track',rawData.GPS_Hemisphere)
     
  if route_name== 1 % 1 means 'test_track';  2 means wahba_loop; 
        start_longitude=-77.833842140800000;
        start_latitude =40.862636161300000;
        start_xEast=1345.204537286125;
        start_yNorth=6190.884280063217;
  elseif route_name == 2
        start_longitude=-77.876520372227550;
        start_latitude =40.828390558947870;
        start_xEast=-2254.319012077573;
        start_yNorth=2387.887818394200;
        %%yaw_angle = 50;
        %%espected distance 
        %route defination for each measurement 
  end
  start_point = [start_latitude, start_longitude,start_xEast,start_yNorth];
 %% break the data into laps 

[lapData,numLaps] = fcn_breakDataIntoLaps_v1(rawData.GPS_Hemisphere,start_point);

%fcn_plot_data_by_laps(lapData,numLaps,start_point,12546); 


%% mean station calculation

%[east_gps_mean,north_gps_mean,Num_laps_mean,station_equidistance] = fcn_mean_station_calculation(lapData,numLaps);

 [aligned_Data_ByStation,mean_Data] = fcn_meanStationProjection_v1(lapData,numLaps-1);
 
 
 
%% calculate offset 
fcn_lateralOffset(aligned_Data_ByStation,mean_Data,numLaps-1)


%% EDITS STOP HERE


%%
h_fig = figure(4856);
set(h_fig,'Name','station and yaw rate ');
plot(lapData{1}.station, lapData{1}.yaw_rate_from_velocity,'b.')
ylim([-5,5])
grid on
        xlabel('Station Distance [m]')
        ylabel('Yaw rate [deg/s]')


%% Plot the bad navMode ENU data by laps
figure(464784);
set(h_fig,'Name','ENU_navMode_in_Laps');
hold on;

% First, plot and label the good data...
legend_string = ''; % Initialize an empty string
for i_Laps = 1:numLaps  
    empty_data = NaN*lapData{i_Laps}.xEast;
    
    goodData_xEast = empty_data;
    goodData_yNorth = empty_data;    
    goodDataIndices = find(lapData{i_Laps}.navMode==6);      
    goodData_xEast(goodDataIndices) = lapData{i_Laps}.xEast(goodDataIndices);
    goodData_yNorth(goodDataIndices) = lapData{i_Laps}.yNorth(goodDataIndices);
   
    plot(goodData_xEast,goodData_yNorth);
    legend_string{i_Laps} = sprintf('Lap %d',i_Laps);
  
end
grid on; 
xlabel('xEast [m]') %set  x label 
ylabel('yNorth [m]') % set y label 
title('Plot of raw ENU data by laps for Wahba loop'); 
legend(legend_string);

% Now plot bad data
%legend_string = ''; % Initialize an empty string
for i_Laps = 1:numLaps  
    empty_data = NaN*lapData{i_Laps}.xEast;
    badData_xEast = empty_data;
    badData_yNorth = empty_data;
    badDataIndices = find(lapData{i_Laps}.navMode~=6);      
    badData_xEast(badDataIndices) = lapData{i_Laps}.xEast(badDataIndices);
    badData_yNorth(badDataIndices) = lapData{i_Laps}.yNorth(badDataIndices);
   
    %plot(badData_xEast,badData_yNorth,'r-','Linewidth',thick_width_Line);
    plot(badData_xEast,badData_yNorth,'Linewidth',thick_width_Line);
    legend_string{i_Laps+numLaps} = sprintf('Bad Lap %d',i_Laps);
  
end
grid on; 
xlabel('xEast [m]') %set  x label 
ylabel('yNorth [m]') % set y label 
title('Plot of raw ENU data by laps for Wahba loop'); 
legend(legend_string);







%%

% filename = 'GPS_left.kml';
% %Write the geographic line data to the file, specifying a description and a name.
% 
% kmlwriteline(filename,GPS_left(1:end,1), GPS_left(1:end,2), GPS_left(1:end,3), ...
%        'Description', 'this is a description', 'Name', 'Track Log','Color','r');
% %    
% % filename2 = 'DGPS_2019_09_17_WahbaLoop_StartTerminal_clampToGround.kml';
% % name2 = 'right_GPS';
% % kmlwriteline(filename2,GPS_right(start_index:end_index,1), GPS_right(start_index:end_index,2), GPS_right(start_index:end_index,3),...
% %     'Name',name2,'Color','b','Width',4, ...
% %     'AltitudeMode','clampToGround');% relativeToSeaLevel,clampToGround





